package com.qaagility.controller;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    private com.qaagility.controller.Calculator calculatorUnderTest;

    @Before
    public void setUp() {
        calculatorUnderTest = new Calculator();
    }

    @Test
    public void testAdd() {
        // Setup

        // Run the test
        final int result = calculatorUnderTest.add();

        // Verify the results
        assertEquals(0, result);
    }
}
